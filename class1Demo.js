let myName;

let myAge = 21;

myAge = 22;
myAge -= 1;

const BEDTIME = 10;

const firstName = 'Matt';
const lastName = 'Schneider';
const fullName = `${firstName} ${lastName}`;
console.log("fullName: ", fullName);

const combinedName = firstName + ' B ' + lastName;
console.log(combinedName);

const quote = 'Can\'t \nStop Me';
console.log(quote);

const weirdNum = 0.2 + 0.1;
console.log(weirdNum.toFixed(2));

console.log(Math.ceil(Math.random()*13));

let rightNow = new Date();
console.log(rightNow);

let tomorrow = new Date(2019, 0, 23);
console.log(tomorrow);


console.log(tomorrow.getTime());
